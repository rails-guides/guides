.视图
* xref:action_view_overview.adoc[Action View 概览 *]
* xref:layouts_and_rendering.adoc[Rails 布局和渲染]
* xref:form_helpers.adoc[Action View 表单构造辅助器]
* xref:action_view_helpers.adoc[Action View 辅助器 *]
